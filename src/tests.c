#define _DEFAULT_SOURCE

#include "tests.h"

void free_heap(size_t query) {
    munmap(HEAP_START, size_max(query, REGION_MIN_SIZE));
}

struct block_header* block_header_from_block(void* block) {
    return block - offsetof(struct block_header, contents);
}

static void *block_after(struct block_header const *block)
{
  return (void *)(block->contents + block->capacity.bytes);
}

bool test1() {
    heap_init(0);
    printf("%s", "ТЕСТ 1: Обычное успешное выделение памяти\n");
    
    uint32_t *dead = _malloc(16);
    if (!dead) {
        printf("%s", "Аллокации не произошло\n");
        return false;
    }
    *dead = 0xADDE0000;
    debug_heap(stdout, HEAP_START);
    
    free_heap(0);
    printf("%s", "\n");

    return true;
}

bool test2() {
    heap_init(0);
    printf("%s", "ТЕСТ 2: Освобождение одного блока из нескольких выделенных\n");
    
    char * word1 = _malloc(32);
    char * word2 = _malloc(48);
    char * word3 = _malloc(28);
    if (!word1 || !word2 || !word3) {
        printf("%s", "Аллокации одного или нескольких блоков не произошло\n");
        return false;
    }
    _free(word2);
    if (!block_header_from_block(word2)->is_free) {
        printf("%s", "Память в блоке не освободилась\n");
        return false;
    }
    if (block_header_from_block(word1)->is_free || block_header_from_block(word3)->is_free) {
        printf("%s", "Освободилась память в соседних блоках\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);

    free_heap(0);
    printf("%s", "\n");

    return true;
}

bool test3() {
    heap_init(0);
    printf("%s", "ТЕСТ 3: Освобождение двух блоков из нескольких выделенных\n");
    
    char * word1 = _malloc(32);
    char * word2 = _malloc(48);
    char * word3 = _malloc(28);
    char * word4 = _malloc(228);
    if (!word1 || !word2 || !word3 || !word4) {
        printf("%s", "Аллокации одного или нескольких блоков не произошло\n");
        return false;
    }
    _free(word1);
    _free(word3);
    if (!block_header_from_block(word1)->is_free || !block_header_from_block(word3)->is_free) {
        printf("%s", "Память в блоке или блоках не освободилась\n");
        return false;
    }
    if (block_header_from_block(word2)->is_free || block_header_from_block(word4)->is_free) {
        printf("%s", "Освободилась память в соседних блоках\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);

    free_heap(0);
    printf("%s", "\n");

    return true;

}

bool test4() {
    heap_init(0);
    printf("%s", "ТЕСТ 4: Память закончилась, новый регион памяти расширяет старый\n");
    
    printf("%s", "Выделяем первый регион:\n");
    char * block1 = _malloc(8175);
    if (!block1) {
        printf("%s", "Память не выделилась\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);

    printf("%s", "Выделяем новый регион:\n");
    char * block2 = _malloc(1);
    if (!block2) {
        printf("%s", "Память не выделилась\n");
        return false;
    }
    if (block_after(block_header_from_block(block1)) != block_header_from_block(block2)) {
        printf("%s", "Новый регион не является продолжением первого\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);
    free_heap(REGION_MIN_SIZE * 2);
    printf("%s", "\n");

    return true;

}

bool test5() {
    heap_init(0);
    printf("%s", "ТЕСТ 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
    
    char * block1 = _malloc(1);
    if (!block1) {
        printf("%s", "Память для первого блока не выделилась\n");
        return false;
    }
    char * another_block = mmap(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    if (another_block == MAP_FAILED) {
        printf("%s", "Не удалось выделить память после кучи\n");
        return false;
    }
    char * block2 = _malloc(REGION_MIN_SIZE);
    if (!block2) {
        printf("%s", "Память для блока, стоящего после выделенной вручную памяти, не выделилась\n");
        return false;
    }
    if (block_after(block_header_from_block(block1)) == block_header_from_block(block2)) {
        printf("%s", "Новый регион является продолжением первого\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);

    printf("%s", "\n");

    return true;
}